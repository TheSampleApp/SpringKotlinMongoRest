package com.the.sample.app.controller

import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/users")
class UserRestController(val userService: UserService) {

    @GetMapping("/{id}")
    fun getUserById(@PathVariable("id") id: String): Mono<User> {
        return userService.findById(id)
    }

    @GetMapping("/")
    fun getAllUsers(): Flux<User> {
        return userService.findAll()
    }

    @PostMapping("/")
    fun saverUser(@RequestBody user: User): Mono<User> {
        userService.save(user)
        return Mono.just(user)
    }

    @PutMapping("/{id}")
    fun updateUser(@PathVariable("id") id: String, @RequestBody user: User): Mono<User>? {
        user.id = id
        userService.save(user)
        return Mono.just(user)
    }

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable("id") id: String): Unit {
        userService.deleteById(id)
    }
}