package com.the.sample.app.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document
class User (
    @Id
    var id: String? = null,
    var fullName: String,
    var email: String
)