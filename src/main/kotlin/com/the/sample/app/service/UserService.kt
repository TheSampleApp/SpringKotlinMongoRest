package com.the.sample.app.service

import com.the.sample.app.model.User
import com.the.sample.app.repository.UserRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

interface UserService {
    fun findAll(): Flux<User>
    fun findById(id: String): Mono<User>
    fun findByEmail(email: String): Mono<User>
    fun save(user: User)
    fun deleteById(id: String)
}

@Service
class UserServiceImpl(val userRepository: UserRepository) : UserService{
    override fun findAll(): Flux<User> {
        return userRepository.findAll()
    }
    override fun findById(id: String): Mono<User> {
        return userRepository.findById(id)
    }

    override fun findByEmail(email: String): Mono<User> {
        return userRepository.findByEmail(email)
    }

    override fun save(user: User) {
        user.id = UUID.randomUUID().toString()
        userRepository.save(user)
    }

    override fun deleteById(id: String) {
        userRepository.deleteById(id)
    }
}